import { atom } from "recoil"

export const test = atom({
  key: 'test', // unique ID (with respect to other atoms/selectors)
  default: "I have value!" // default value (aka initial value)
});
