import React from 'react';
import Index from './index.js';
import { RecoilRoot } from 'recoil';

export default function App() {
  return (
    <RecoilRoot>
      <Index />
    </RecoilRoot>
  )
}