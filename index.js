import * as Location from 'expo-location';
import * as React from 'react';
import MapView, { Marker } from 'react-native-maps';
import { StyleSheet, Text, View, Dimensions } from 'react-native';
import { useRecoilState } from 'recoil';
import { test } from './atoms/test.js';

export default function Index() {
  const [location, setLocation] = React.useState({
    latitude: 0,
    longitude: 0
  });
  const [center, setCenter] = React.useState({
    latitude: 0,
    longitude: 0
  });
  const [errorMsg, setErrorMsg] = React.useState(null);
  const [content, setContent] = useRecoilState(test);

  const markers = [0, 1]

  React.useEffect(() => {
    (async () => {
      let { status } = await Location.requestForegroundPermissionsAsync();
      if (status !== 'granted') {
        setErrorMsg('Permission to access location was denied');
        return;
      }
      let location = await Location.getCurrentPositionAsync({});
      setLocation(location.coords);
      setCenter(location.coords);
    })();
  }, []);

  let text = 'Waiting..';
  if (errorMsg) {
    text = errorMsg;
  } else if (location) {
    text = JSON.stringify(location);
  }

  if (location.latitude) {
    return (
      <View style={styles.container}>
        <MapView style={styles.map} initialRegion={{
          latitude: location.latitude,
          longitude: location.longitude,
          latitudeDelta: 0.0922,
          longitudeDelta: 0.0421,
        }}
          provider="google"
        // onRegionChange={region => {
        //   setLocation(region)
        // }}
        >
          {markers.map((marker, i) => {
            return i === 0 ? <Marker
              key={marker}
              coordinate={{ latitude: center.latitude, longitude: center.longitude }}
            // image={{ uri: 'custom_pin' }}
            />
              : <Marker
                key={marker}
                coordinate={{ latitude: center.latitude + 0.01, longitude: center.longitude + 0.01 }}
              // image={{ uri: 'custom_pin' }}
              />
          })}
        </MapView>
      </View>
    );
  } else {
    return <View style={styles.container}>
      <Text>Fetching info...</Text>
    </View>
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  map: {
    width: Dimensions.get('window').width,
    height: Dimensions.get('window').height,
  },
  paragraph: {
    fontSize: 18,
    textAlign: 'center',
  },
});